#encoding: utf-8
require 'rubygems'
require 'bundler/setup'
Bundler.require
require_relative 'settings'
require_relative 'tools'

class DataAgentTask

  PATH = File.dirname(Pathname.new(__FILE__).realpath.to_path)
  VERSION = "2.5.0 test"


  def self.run
    count = 1
    data_agent_task = DataAgentTask.new
    process_file = File.join(File.dirname(PATH), "log", "process.lock")
    get_task_result = JSON.parse(File.read('task.json'))

    count.times do |i|
      begin
        p "开始第#{i+1}次任务"

        # 1. 获取任务内容
        p "开始获取任务内容"
        p "获取任务内容成功：#{get_task_result}"

        get_task_result["tasks"].each do |task|
          # 2. 执行任务
          if task["task_type"] == "click"#点击任务
            p "开始执行点击任务"
            data_agent_task.click(task)
            p "执行点击任务成功"
          elsif task["task_type"] == "impression"#曝光任务
            p "开始执行曝光任务"
            data_agent_task.impression(task)
            p "执行曝光任务成功"
          elsif task["task_type"] == "click_with_device_id"#点击任务（带设备号）
            p "开始执行点击任务（带设备号）"
            if task["device"]["device_id"].to_s.length > 2
              data_agent_task.click_with_device_id(task)
              p "执行点击任务（带设备号）成功".green  
            else
              p "执行点击任务（带设备号）失败，没有设备号".red
              sleep 5
              next
            end
          elsif task["task_type"] == "impression_with_device_id"#曝光任务（带设备号）
            p "开始执行曝光任务（带设备号）"
            if task["device"]["device_id"].to_s.length > 2
              data_agent_task.impression_with_device_id(task)
              p "执行曝光任务（带设备号）成功"
            else
              p "执行曝光任务（带设备号）失败，没有设备号"
              sleep 5
              next
            end
          else
            next         
          end

        end
        p "第#{i+1}次任务完成"
      rescue => e
        `killall chrome`
        p "第#{i+1}次任务失败：#{e}"
      end
    end
  end


  def click(task)
    options = Selenium::WebDriver::Chrome::Options.new
    options.add_emulation(user_agent: task["useragent"]["content"])
    options.add_argument("disk-cache-dir=#{File.join(File.dirname(PATH),'cache')}") if rand(10) > 3
    if task["browser_type"] == "app" or task["browser_type"] == "mobile"
      options.add_emulation(device_metrics: {width: 375, height: 667, pixelRatio: 2, touch: true})
    end

    driver = Selenium::WebDriver.for(:chrome, options: options)
    browser = Watir::Browser.new driver

    if task["browser_type"] == "app" or task["browser_type"] == "mobile"
      browser.window.resize_to(750, 1334)
    elsif task["browser_type"] == "pc"
      browser.window.resize_to(1900, 950)
    end

    begin
      source_url = task["source_url"].split("&&").shuffle[0]
      residence_time = task["residence_time"].to_i * (1+rand(-0.2..0.2))
      all_residence_time = 110

      Timeout.timeout(all_residence_time, Errno::ETIMEDOUT) do
        #曝光
        p "曝光"
        if task["impression_url"] != "" and task["impression_url"] != nil
          if source_url != "" and source_url != nil
            open(task["impression_url"], "User-Agent" => task["useragent"]["content"], "Referer" => source_url)
          else
            open(task["impression_url"], "User-Agent" => task["useragent"]["content"])
          end
          sleep rand(1..3)
        end

        #点击
        p "点击"
        if task["click_url"] != "" and task["click_url"] != nil
          begin
            Timeout.timeout(rand(29..31), Errno::ETIMEDOUT) do
              browser.goto task["click_url"]
            end
          rescue => e
          ensure
          end
          sleep residence_time
        end

        #后续操作
        p "后续操作"
        window_id = `xdotool search #{task["window_name"]}`.split("\n")[0]
        task["operations"].each do |operation|
          break if rand(100) > operation["probability"]
          if operation["operation_type"] == "js"
            browser_js(browser, operation["content"])
          elsif operation["operation_type"] == "mouse_click"
            arg = operation["content"].split("&&")
            mouse_click(window_id, arg[0].to_i, arg[1].to_i, arg[2].to_i, arg[3].to_i)
          elsif operation["operation_type"] == "mouse_move"
            arg = operation["content"].split("&&")
            mouse_move(window_id, arg[0].to_i, arg[1].to_i, arg[2].to_i, arg[3].to_i)
          elsif operation["operation_type"] == "keyboard_down"
            arg = operation["content"].to_i
            keyboard_down(arg)
          end
          operation_residence_time = operation["residence_time"].to_i * (1+rand(-0.5..0.5))
          sleep operation_residence_time
        end

        #重复执行
        p "重复执行"
        if rand(100) > 85
          if task["impression_url"] != "" and task["impression_url"] != nil#曝光
            if source_url != "" and source_url != nil
              open(task["impression_url"], "User-Agent" => task["useragent"]["content"], "Referer" => source_url)
            else
              open(task["impression_url"], "User-Agent" => task["useragent"]["content"])
            end
            sleep rand(1..3)
          end

          if task["click_url"] != "" and task["click_url"] != nil#点击
            begin
              Timeout.timeout(rand(18..22), Errno::ETIMEDOUT) do
                browser.goto task["click_url"]
              end
            rescue => e
            ensure
            end
            sleep rand(2..3)
          end
        end

      end
    rescue => e
      p "执行点击任务失败：#{e}"
    ensure
      `killall chrome`
    end
  end


  def impression(task)
    begin
      impression_rate = task["impression_rate"].to_i + [-2,-1,-1,0,0,0,0,1,1,2].sample
      source_url = task["source_url"].split("&&").shuffle[0]
      all_residence_time = 60

      Timeout.timeout(all_residence_time, Errno::ETIMEDOUT) do
        for m in 1..impression_rate
          if source_url != "" and source_url != nil
            open(task["impression_url"], "User-Agent" => task["useragent"]["content"], "Referer" => source_url)
          else
            open(task["impression_url"], "User-Agent" => task["useragent"]["content"])
          end
          sleep rand(4..6)
        end
      end
    rescue => e
      p "执行曝光任务失败：#{e}"
    end
  end


  def click_with_device_id(task)
    return if task["device"]["device_id"].to_s.length < 2

    if task["device"]["app_type"] == "iOS"
      impression_url = task["impression_url"].to_s.gsub("__IDFA__", task["device"]["device_id"])
      click_url = task["click_url"].to_s.gsub("__IDFA__", task["device"]["device_id"])
    elsif task["device"]["app_type"] == "android"
      impression_url = task["impression_url"].to_s.gsub("__IMEI__", task["device"]["device_id"])
      click_url = task["click_url"].to_s.gsub("__IMEI__", task["device"]["device_id"])
    else
      return
    end

    options = Selenium::WebDriver::Chrome::Options.new
    options.add_emulation(user_agent: task["useragent"]["content"])
    options.add_argument("disk-cache-dir=#{File.join(File.dirname(PATH),'cache')}") if rand(10) > 3
    if task["browser_type"] == "app" or task["browser_type"] == "mobile"
      options.add_emulation(device_metrics: {width: 375, height: 667, pixelRatio: 2, touch: true})
    end

    driver = Selenium::WebDriver.for(:chrome, options: options)
    browser = Watir::Browser.new driver

    if task["browser_type"] == "app" or task["browser_type"] == "mobile"
      browser.window.resize_to(750, 1334)
    elsif task["browser_type"] == "pc"
      browser.window.resize_to(1900, 950)
    end

    begin
      source_url = task["source_url"].split("&&").shuffle[0]
      residence_time = task["residence_time"].to_i * (1+rand(-0.2..0.2))
      all_residence_time = 110

      Timeout.timeout(all_residence_time, Errno::ETIMEDOUT) do
        #曝光
        p "曝光"
        if impression_url != "" and impression_url != nil
          if source_url != "" and source_url != nil
            open(impression_url, "User-Agent" => task["device"]["useragent"].to_s, "Referer" => source_url)
          else
            open(impression_url, "User-Agent" => task["device"]["useragent"].to_s)
          end
          sleep rand(1..3)
        end

        #点击
        p "点击"
        if click_url != "" and click_url != nil
          begin
            Timeout.timeout(rand(29..31), Errno::ETIMEDOUT) do
              browser.goto click_url
            end
          rescue => e
          ensure
          end
          sleep residence_time
        end

        #后续操作
        p "后续操作"
        window_id = `xdotool search #{task["window_name"]}`.split("\n")[0]
        task["operations"].each do |operation|
          break if rand(100) > operation["probability"]
          if operation["operation_type"] == "js"
            browser_js(browser, operation["content"])
          elsif operation["operation_type"] == "mouse_click"
            arg = operation["content"].split("&&")
            mouse_click(window_id, arg[0].to_i, arg[1].to_i, arg[2].to_i, arg[3].to_i)
          elsif operation["operation_type"] == "mouse_move"
            arg = operation["content"].split("&&")
            mouse_move(window_id, arg[0].to_i, arg[1].to_i, arg[2].to_i, arg[3].to_i)
          elsif operation["operation_type"] == "keyboard_down"
            arg = operation["content"].to_i
            keyboard_down(arg)
          end
          operation_residence_time = operation["residence_time"].to_i * (1+rand(-0.5..0.5))
          sleep operation_residence_time
        end

        #重复执行
        p "重复执行"
        if rand(100) > 85
          if impression_url != "" and impression_url != nil#曝光
            if source_url != "" and source_url != nil
              open(impression_url, "User-Agent" => task["device"]["useragent"].to_s, "Referer" => source_url)
            else
              open(impression_url, "User-Agent" => task["device"]["useragent"].to_s)
            end
            sleep rand(1..3)
          end

          if click_url != "" and click_url != nil#点击
            begin
              Timeout.timeout(rand(18..22), Errno::ETIMEDOUT) do
                browser.goto click_url
              end
            rescue => e
            ensure
            end
            sleep rand(2..3)
          end
        end

      end
    rescue => e
     p "执行点击任务失败：#{e}"
    ensure
     `killall chrome`
    end
  end


  def impression_with_device_id(task)
    begin
      return if task["device"]["device_id"].to_s.length < 2

      impression_rate = task["impression_rate"].to_i + [-2,-1,-1,0,0,0,0,1,1,2].sample
      source_url = task["source_url"].split("&&").shuffle[0]
      if task["device"]["app_type"] == "iOS"
        impression_url = task["impression_url"].gsub("__IDFA__", task["device"]["device_id"])
      elsif task["device"]["app_type"] == "android"
        impression_url = task["impression_url"].gsub("__IMEI__", task["device"]["device_id"])
      else
        impression_url = task["impression_url"]
      end
      all_residence_time = 60

      Timeout.timeout(all_residence_time, Errno::ETIMEDOUT) do
        for m in 1..impression_rate

          if source_url != "" and source_url != nil
            open(impression_url, "User-Agent" => task["device"]["useragent"], "Referer" => source_url)
          else
            open(impression_url, "User-Agent" => task["device"]["useragent"])
          end
          sleep rand(4..6)
        end
      end
    rescue => e
      p "执行曝光任务失败：#{e}"
    end
  end


  def impression_with_dongfeng(task)
    begin
      return if task["device"]["device_id"].to_s.length < 2

      impression_rate = task["impression_rate"].to_i + [-2,-1,-1,0,0,0,0,1,1,2].sample
      source_url = task["source_url"].split("&&").shuffle[0]
      device_id = task["device"]["device_id"]
      if task["device"]["app_type"] == "iOS"
        response = Tools.http_request method: :post, url: "http://tanx.qdaily.com/tanx/splash_do", params: {ios_pid: "mm_26632771_24592098_82770849", width: "750", height: "1334", device:{device_type: "0", idfa: device_id, network: "1", os: "iOS", osv: "11.0", user_agent: ""}}
        impression_urls = response[:impression_tracking_url] ? response[:impression_tracking_url].collect{|item| item.gsub("__IDFA__", device_id)} : []
      elsif task["device"]["app_type"] == "android"
        response = Tools.http_request method: :post, url: "http://tanx.qdaily.com/tanx/splash_do", params: {android_pid: "mm_26632771_24592098_142562160", width: "1080", height: "1920", device:{device_type: "0", imei: device_id, network: "1", os: "Android", osv: "7.1.1", user_agent: ""}}
        impression_urls = response[:impression_tracking_url] ? response[:impression_tracking_url].collect{|item| item.gsub("__IMEI__", device_id)} : []
      else
        return
      end

      all_residence_time = 60

      Timeout.timeout(all_residence_time, Errno::ETIMEDOUT) do
        for m in 1..impression_rate

          if source_url != "" and source_url != nil
            impression_urls.each{|impression_url| open(impression_url, "User-Agent" => task["device"]["useragent"].to_s, "Referer" => source_url)}
          else
            impression_urls.each{|impression_url| open(impression_url, "User-Agent" => task["device"]["useragent"].to_s)}
          end
          sleep rand(4..6)
        end
      end
    rescue => e
      p "执行曝光任务失败：#{e}"
    end
  end


  def browser_js(browser, content)
    begin
      Timeout.timeout(rand(14..16), Errno::ETIMEDOUT) do
        browser.execute_script content#执行js
      end
    rescue => e
    end
  end


  def mouse_click(window_id, x_begin, y_begin, x_radius, y_radius)
    x_offset_set = []
    for i in 0..x_radius
      for j in 1..(x_radius+1-i)
        x_offset_set << i
      end
    end
    x_offset = x_offset_set.shuffle[0]
    x = [x_begin - x_offset, x_begin + x_offset].shuffle[0]

    y_offset_set = []
    for i in 0..y_radius
      for j in 1..(y_radius+1-i)
        y_offset_set << i
      end
    end
    y_offset = y_offset_set.shuffle[0]
    y = [y_begin - y_offset, y_begin + y_offset].shuffle[0]

    `xdotool mousemove --window #{window_id} #{x} #{y} click 1`
  end


  def mouse_move(window_id, x_begin, y_begin, x_end, y_end)
    `xdotool mousemove --window #{window_id} #{x_begin} #{y_begin} mousedown 1 sleep 0.5 mousemove #{x_end} #{y_end} mouseup 1`
  end


  def keyboard_down(click_count)
    for i in 1..click_count
      `xdotool key Down`
      sleep 0.5
    end
  end


end


DataAgentTask.run


