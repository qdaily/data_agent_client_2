#encoding: utf-8

class Tools

  def self.check_performance
    result = (Vmstat.snapshot.cpus.count - Vmstat.snapshot.load_average.one_minute <= 0.5)
  end


  def self.check_performance_and_wait
    100.times do |i|
      if Tools.check_performance
        sleep 3
      else
        break
      end
    end
  end


  def self.http_request(method: :get, url: , params: {})
    begin
      http_res = RestClient::Request.execute(method: method, url: url, payload: params, timeout: 60)
      JSON.parse(http_res.body, :symbolize_names => true )
    rescue
      nil
    end
  end


  def self.check_process_exist(process_file)
    if File.exists?(process_file)
      count = Tools.get_count(process_file).to_i
      if count > 30
        Tools.set_count(process_file)
        result = false
      else
        Tools.set_count(process_file, count+1)
        result = true
      end
    else
      %x{touch #{process_file}}
      Tools.set_count(process_file)
      result = false
    end
    return result
  end


  def self.delete_process_file(process_file)
    File.delete(process_file) if File.exists?(process_file)
  end


  private


  def self.get_count(process_file)
      File.open(process_file, "r") {|f|
        f.flock(File::LOCK_SH)
        number = f.read
      }
  end


  def self.set_count(process_file, number=1)
      File.open(process_file, File::RDWR|File::CREAT, 0644) {|f|
        f.flock(File::LOCK_EX)
        f.rewind
        f.write(number)
        f.flush
        f.truncate(f.pos)
      }
  end

end