#encoding: utf-8
require 'rubygems'
require 'bundler/setup'
Bundler.require
require_relative 'settings'
require_relative 'tools'

class UpdateCode

  def self.run
    # 1. 随机等待0～1分钟
    sleep rand(1..60)

    # 2. 更新代码
    for m in 1..10
      success = false
      begin
        Timeout.timeout(20, Errno::ETIMEDOUT) do
          `sudo git pull`
          success = true
        end
      rescue => e
        sleep 5
      end
      break if success
    end
  end


end
