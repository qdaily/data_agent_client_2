require_relative File.expand_path("../../restart_server", __FILE__)
ENV['DISPLAY'] = ":1"
desc "run restart server process"
task :run_restart_server do
  RestartServer.run
end