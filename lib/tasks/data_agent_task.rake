require_relative File.expand_path("../../data_agent_task", __FILE__)
ENV['DISPLAY'] = ":1"
desc "run data agent task process"
task :run_data_agent_task do
  DataAgentTask.run
end